Carroyage
=========

![](screenshot.png)

* une version française de ce texte est disponible en dessous

English
-------

### Description

This QGIS plugin allows you to generate automatically grids on a map.
Each tile generated is a polygon with attributes automatically set
allowing easy assignment of labels or spatial queries.

This plugin differs from the classic grid generation by simplifying the
orientation of the grid and generating the corresponding attributes.

### Usage

Before launching the plugin itself it is necessary to select a QGIS layer and
an axis that will orient our grid. This axis is a simple line. In the plugin 
context, this line will correspond to an X or Y axis in the plan defined by 
our grid.

Once this selection is made, launch the plugin (via the menu "Vectors >
Archeology > Grid" or via the associated button). Several options are 
available do not hesitate to use the preview before validating. The different
available options are:
* Edge size: by default the size of the edge corresponds to the size
of the selected line. This size is expressed in the unit used on
the layer containing the selected line. The distances used being
a few meters, distance calculations are done considering themselves on a
plan.
* Ratio rows / columns: if your tiles are not square you can change the ratio 
here.
* Selected axis: change this parameter if the selected line matches
to the Y axis of your grid.
* Direction: with this parameter you can change the direction of the axis of
your grid.
* Number (rows, columns): the desired number of rows (or columns) in your grid.
* Numbering type: two types of numbering are available numeric (1, 2, 3, ...)
 or alpha (A, B, C, ...). For alphabetical numbering when exceeding 26, the 
 numbering continues with AA, AB, AC, ...
* Numbering order: it is possible to dial starting from the end, for that 
choose: "desc".
* Starting number: number to start by. If the numbering is alphabetical, you 
have to retranscribe in number the departure (for example if you start with 
Z, you have to put 26).

Once the right settings found pressing OK will create a new layer with your 
grid.

*Warning*: the new created layer is a temporary layer in memory, it is 
necessary to save it.


Français
--------

### Description

Ce greffon QGIS permet de générer des carroyages automatiquement sur un plan.
Chaque carreau généré est un polygone disposant automatiquement d'attributs
permettant facilement l'affectation d'étiquettes ou des requêtes spatiales.

Ce greffon se différencie de la fonctionnalité Grille vecteur en simplifiant 
l'orientation de la grille et en générant les attributs correspondants.
 
Il a été développé en réponse à des besoins spécifiques à la recherche archéologique, 
où le carroyage est régulièrement utilisé comme unité d'enregistrement, 
mais peut correspondre à différents besoins de recherche pour permettre un 
traitement statistique.

### Utilisation

Avant de lancer le greffon proprement dit, il est nécessaire de sélectionner une
couche et une ligne de cette couche, dont l'extrémité donnera l'origine du carroyage 
et l'axe donnera l'orientation du carroyage. Cette ligne peut correspondre aussi 
bien à l'axe X qu'à l'axe Y du carroyage.

Une fois cette sélection faite, lancer le greffon (via le menu « Vecteurs > 
Outils de recherche > Carroyage » ou via le bouton associé). Plusieurs options 
sont proposées. Il ne faut pas hésiter à utiliser l'aperçu afin de tester les 
différentes options avant de valider. 

Les options disponibles sont :
* Taille de l'arête : par défaut la taille de l'arête correspond à la longueur 
de la ligne sélectionnée. Cette taille est exprimée dans l'unité utilisée sur 
la couche contenant la ligne sélectionnée. Les distances utilisées étant 
classiquement de quelques mètres, les calculs de distances sont faits en se 
considérant sur un plan (pas de prise en compte de la projection / du SRC). 
Dans le cas classique français (coordonnées Lambert ou locales, carroyage constitué 
de carrés d'1m de côté), il faut donc mettre cette option à 1. 
* Ratio lignes / colonnes : il est possible de réaliser un carroyage avec des unités
rectangulaires plutôt que carrées en modifiant ce ratio (par défaut à 1:1).
* Axe sélectionné : par défaut à X. Modifier si la ligne sélectionnée correspond 
à l'axe Y du carroyage.
* Direction : permet de définir si c'est l'extrémité gauche ou droite de la ligne 
sélectionnée qui définit l'origine. Par défaut, c'est l'extrémité gauche.
* Nombre (lignes, colonnes) : le nombre de lignes (ou de colonnes) souhaité 
dans le carroyage.
* Type de numérotation : deux types de numérotations sont disponibles, 
numérique (1, 2, 3, ...) ou alphabétique (A, B, C, ...). Pour la numérotation 
alphabétique, lorsque l'on dépasse 26, la numérotation se poursuit avec AA, AB, AC, ...
* Ordre de numérotation : il est possible de numéroter suivant un ordre décroissant. 
Par défaut en ordre croissant.
* Nombre de départ : nombre par lequel commencer. Si la numérotation est 
alphabétique, il faut retranscrire le début de cette numérotation par son équivalent 
numérique (mettre 1 si l'on commence à A, 10 si l'on commence à J, 26 si l'on commence à Z).

Une fois les bons paramètres trouvés, appuyer sur OK créera une nouvelle couche
avec le carroyage.

*Attention* : la nouvelle couche créée est une couche temporaire en mémoire, 
il est nécessaire de la sauvegarder.

Le fichier résultant contient 5 champs :
* *X label* et *Y label* contiennent les éléments demandés par l'utilisateur comme 
numérotation.
* *X* et *Y* contiennent la version numérique de ces éléments.
* *Label* correspond à la concaténation des champs *X label* et *Y label*. Dans les cas 
« classiques », on dispose ainsi directement des noms des différentes unités de notre 
carroyage.
