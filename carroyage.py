#! /usr/bin/python3
# -*- coding: utf-8 -*-


"""
/***************************************************************************
 Carroyage
                                 A QGIS plugin
 This QGIS plugin allows you to generate automatically grids on a map.
                              -------------------
        begin                : 2017-12-16
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Iggdrasil
        email                : etienne.loks@iggdrasil.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from __future__ import absolute_import

from builtins import str
from builtins import range
from math import sqrt

from qgis.PyQt.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, QVariant, pyqtSlot, QObject
from qgis.PyQt.QtWidgets import QAction, QMessageBox
from qgis.PyQt.QtGui import QIcon
# Initialize Qt resources from file resources.py
from . import resources
# Import the code for the dialog
from .carroyage_dialog import CarroyageDialog

from qgis.core import QgsVectorLayer, QgsFeature, QgsField, QgsGeometry, \
    QgsPointXY, QgsProject

import os.path


class Carroyage(QObject):
    """QGIS Plugin Implementation."""

    NUMERATE_TYPE = [
        'numeric',
        'alpha'
    ]
    ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        super().__init__()
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        self.init_localisation()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr('&Archaeology')
        self.toolbar = self.iface.addToolBar('Archaeology')
        # ui can be with no toolbar on test
        if self.toolbar:
            self.toolbar.setObjectName('Archaeology')
        self.message_box = QMessageBox()

        self.layer = None  # result layer
        self.selection = None  # vector selected
        self.reference_layer = None  # layer of the vector
        self.test = False  # used for test context

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('Archaeology', message)

    def init_localisation(self):
        """
        Manage locale initialization.
        On testing the locale is not found: skip this.
        """
        settings = QSettings()
        if not settings:
            return
        locale = settings.value('locale/userLocale')
        if not locale:
            return
        locale = locale[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'carroyage_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = CarroyageDialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar and self.toolbar:
            self.toolbar.addAction(action)

        if add_to_menu and hasattr(self.iface, "addPluginToVectorMenu"):
            self.iface.addPluginToVectorMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """
        Create the menu entries and toolbar icons inside the QGIS GUI.
        Connect the preview button.
        """

        icon_path = ':/plugins/carroyage/icon.png'
        self.add_action(
            icon_path,
            text=self.tr('Carroyage'),
            callback=self.run,
            parent=self.iface.mainWindow())

        self.dlg.preview_button.clicked.connect(self.preview)

    def unload(self):
        """
        Removes the plugin menu item and icon from QGIS GUI.
        """
        for action in self.actions:
            if hasattr(self.iface, "removePluginToVectorMenu"):
                self.iface.removePluginVectorMenu(
                    self.tr('&Archaeology'),
                    action)
            if hasattr(self.iface, "removeToolBarIcon"):
                self.iface.removeToolBarIcon(action)
        if self.toolbar:
            # remove the toolbar
            del self.toolbar

    @pyqtSlot()
    def preview(self):
        """
        Render the preview.
        """
        self.draw_carroyage()
        if self.test:
            return
        self.reselect()

    def remove_preview(self):
        """
        Clean the preview.
        """
        if self.layer:
            if self.layer.id() in self.project.mapLayers():
                self.project.removeMapLayer(self.layer)
            self.layer = None

    def get_selection(self):
        """
        Check and get the selected axis.
        """
        if self.selection:
            return self.selection
        if not self.test:  # reference layer set manually on test
            self.reference_layer = self.iface.activeLayer()
        if not self.reference_layer:
            self.show_error('no-line')
            return
        selection = self.reference_layer.selectedFeatures()
        if not selection:
            self.show_error('no-line')
            return

        if len(selection) > 1:
            self.show_error('multiple')
            return
        return selection[0]

    def draw_carroyage(self):
        """
        Main rendering.
        """
        if not self.get_values():
            self.show_error()
            return
        if not self.selection:
            self.selection = self.get_selection()
        if not self.selection:
            return
        geom = self.selection.geometry()
        if geom.wkbType() != 2:  # LineString
            self.show_error('multiple')
            return
        if geom.isMultipart():
            geom = geom.asMultiPolyline()[0]
        else:
            geom = geom.asPolyline()
        self.init_vectors(geom)

        self.project = QgsProject.instance()
        self.remove_preview()

        name = "Carroyage"
        crs = "EPSG:{}".format(self.reference_layer.crs().postgisSrid())
        self.layer = QgsVectorLayer("Polygon?crs=" + crs, name, "memory")

        data_provider = self.layer.dataProvider()
        data_provider.addAttributes([
            QgsField("Label", QVariant.String, 'string'),
            QgsField("X", QVariant.Int, 'integer'),
            QgsField("Y", QVariant.Int, 'integer'),
            QgsField("X label", QVariant.String, 'string'),
            QgsField("Y label", QVariant.String, 'string'),
        ])
        self.layer.updateFields()

        feats = self.draw_tiles()
        data_provider.addFeatures(feats)
        self.project.addMapLayer(self.layer)

    def show_error(self, error_type='unknown'):
        """
        Error helper.
        """
        if error_type == 'no-line':
            message = self.tr(
                "No line selected. Before launching the plugin, "
                "it is necessary to select a QGIS layer and a line. This "
                "line will be used as an axis to orient our grid.")
        elif error_type == 'multiple':
            message = self.tr(
                "Invalid feature selected. Select only one line.")
        else:
            message = self.tr("Unknown error.")
        self.message_box.setText(message)
        self.message_box.show()
        return

    def get_values(self):
        """
        Get values from inputs.
        """
        self.axis_name = self.dlg.axis_input.currentIndex() and "Y" or "X"
        self.axis_direction = self.dlg.direction_input.currentIndex() and \
                              "R->L" or "L->R"

        self.size = float(self.dlg.size_input.value())
        self.number_x = int(self.dlg.number_x_input.value())
        self.number_y = int(self.dlg.number_y_input.value())
        self.rows = int(self.dlg.row_nb_input.value())
        self.row_type = self.NUMERATE_TYPE[
            self.dlg.row_type_input.currentIndex()]
        self.row_order = self.dlg.row_order_input.currentIndex() and 'desc' \
                         or 'asc'
        self.row_starting = int(self.dlg.row_starting_input.value())
        self.cols = int(self.dlg.col_nb_input.value())
        self.col_type = self.NUMERATE_TYPE[
            self.dlg.col_type_input.currentIndex()]
        self.col_order = self.dlg.col_order_input.currentIndex() and 'desc' \
                         or 'asc'
        self.col_starting = int(self.dlg.col_starting_input.value())
        return True

    def get_default_len(self, axis):
        """
        Initialize the size input with the len of the selected line.
        """
        base = axis[0]
        vect_direction = axis[1]
        vector_1 = (vect_direction[0] - base[0],
                    vect_direction[1] - base[1])
        len_vector = sqrt(vector_1[0] ** 2 + vector_1[1] ** 2)
        # the default size is the len of the selected vector
        self.dlg.size_input.setValue(len_vector)

    def init_vectors(self, axis):
        """
        Init translation vectors from a selected axis.
        """
        diff = axis[1][0] - axis[0][0]
        if (self.axis_direction == "L->R" and diff >= 0) or (
                self.axis_direction == "R->L" and diff <= 0):
            self.base = axis[0]
            vect_direction = axis[1]
        else:
            self.base = axis[1]
            vect_direction = axis[0]

        vector_1 = (vect_direction[0] - self.base[0],
                    vect_direction[1] - self.base[1])
        if self.axis_name == "Y":
            # -90° rotation
            vector_1 = (vector_1[1], -vector_1[0])

        len_vector = sqrt(vector_1[0] ** 2 + vector_1[1] ** 2)

        # normalize the vector
        self.vector_1 = (vector_1[0] / len_vector,
                         vector_1[1] / len_vector)
        self.vector_2 = (- self.vector_1[1], self.vector_1[0])

    def numeric_label(self, value):
        """
        Display numeric label.
        """
        return str(value)

    def alpha_label(self, value):
        """
        Display alpha label (A, B, C...).
        """
        values = []
        while value > 0:
            value -= 1
            values.append(value % 26)
            value = value // 26
        lbl = "".join([self.ALPHA[v] for v in reversed(values)])
        return lbl

    def get_attributes(self, x_index, y_index):
        """
        Get all the attributes from x and y positions.
        """
        if x_index is None:
            x_index = 0
        if self.col_order == 'desc':
            x_index = self.cols - 1 - x_index - self.number_x
        x_index = x_index // self.number_x + self.col_starting

        if y_index is None:
            y_index = 0
        if self.row_order == 'desc':
            y_index = self.rows - 1 - y_index - self.number_y
        y_index = y_index // self.number_y + self.row_starting

        x_label = getattr(self, self.col_type + '_label')(x_index)
        y_label = getattr(self, self.row_type + '_label')(y_index)
        label = "{}{}".format(x_label, y_label)
        return [label, x_index, y_index, x_label, y_label]

    def draw_tiles(self):
        """
        Tiles render
        """
        feats = []
        vector_1 = [
            self.vector_1[0] * self.size * self.number_x,
            self.vector_1[1] * self.size * self.number_x
        ]
        vector_2 = [
            self.vector_2[0] * self.size * self.number_y,
            self.vector_2[1] * self.size * self.number_y
        ]

        len_x_index = self.cols
        len_y_index = self.rows
        for x_index in range(len_x_index):
            for y_index in range(len_y_index):

                new_feature = QgsFeature()
                new_feature.setAttributes(
                    self.get_attributes(x_index, y_index))
                base_coord = (
                    self.base[0] + x_index * vector_1[0] +
                    y_index * vector_2[0],
                    self.base[1] + x_index * vector_1[1] +
                    y_index * vector_2[1]
                )

                second_coord = (
                    self.base[0] + ((x_index + 1) * vector_1[0] +
                                    y_index * vector_2[0]),
                    self.base[1] + ((x_index + 1) * vector_1[1] +
                                    y_index * vector_2[1])
                )

                opposite_coord = (
                    base_coord[0] + vector_1[0] +
                    vector_2[0],
                    base_coord[1] + vector_1[1] +
                    vector_2[1],
                )

                last_coord = (
                    self.base[0] + (x_index * vector_1[0] +
                                    (y_index + 1) * vector_2[0]),
                    self.base[1] + (x_index * vector_1[1] +
                                    (y_index + 1) * vector_2[1])
                )

                polygon = [
                    QgsPointXY(*base_coord),
                    QgsPointXY(*second_coord),
                    QgsPointXY(*opposite_coord),
                    QgsPointXY(*last_coord),
                    QgsPointXY(*base_coord),
                ]

                geom = QgsGeometry.fromPolygonXY([polygon])
                new_feature.setGeometry(geom)
                feats.append(new_feature)
        return feats

    def reselect(self):
        """
        Reselect the axis feature
        """
        if self.reference_layer and self.selection:
            self.iface.setActiveLayer(self.reference_layer)
            self.reference_layer.selectByIds([self.selection.id()])

    def reinit(self):
        self.reselect()
        self.layer = None
        self.selection = None

    def run(self):
        """
        Run method that performs all the real work
        """
        self.selection = self.get_selection()
        if not self.selection:
            return
        geom = self.selection.geometry()
        if geom.wkbType() != 2:  # LineString
            self.show_error('multiple')
            return
        if geom.isMultipart():
            geom = geom.asMultiPolyline()[0]
        else:
            geom = geom.asPolyline()
        self.get_default_len(geom)
        # show the main dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if not result:
            self.remove_preview()
            return
        self.draw_carroyage()
        self.reinit()
