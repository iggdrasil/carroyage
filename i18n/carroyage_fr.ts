<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Carroyage</name>
    <message>
        <location filename="carroyage.py" line="220"/>
        <source>&amp;Archaeology</source>
        <translation type="unfinished">&amp;Archéologie</translation>
    </message>
    <message>
        <location filename="carroyage.py" line="205"/>
        <source>Carroyage</source>
        <translation type="unfinished">Carroyage</translation>
    </message>
    <message>
        <location filename="carroyage.py" line="309"/>
        <source>No line selected. Before launching the plugin, it is necessary to select a QGIS layer and a line. This line will be used as an axis to orient our grid.</source>
        <translation type="unfinished">Pas de ligne sélectionnée. Avant de lancer le greffon, il est nécessaire de sélectionner un calque et une ligne. Cette ligne va être utilisée comme un axe pour orienter votre carroyage.</translation>
    </message>
    <message>
        <location filename="carroyage.py" line="314"/>
        <source>Multiples item selected. Select only one line.</source>
        <translation type="unfinished">Plusieurs éléments sélectionnés. Sélectionnez seulement une ligne.</translation>
    </message>
    <message>
        <location filename="carroyage.py" line="316"/>
        <source>Unknown error.</source>
        <translation type="unfinished">Erreur inconnue.</translation>
    </message>
</context>
<context>
    <name>CarroyageDialogBase</name>
    <message>
        <location filename="carroyage_dialog_base.ui" line="26"/>
        <source>Carroyage</source>
        <translation type="unfinished">Carroyage</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="60"/>
        <source>Ratio rows / colums</source>
        <translation type="unfinished">Ratio lignes / colonnes</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="82"/>
        <source>/</source>
        <translation type="unfinished">/</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="104"/>
        <source>Selected axis</source>
        <translation type="unfinished">Axe sélectionné</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="112"/>
        <source>X</source>
        <translation type="unfinished">X</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="117"/>
        <source>Y</source>
        <translation type="unfinished">Y</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="126"/>
        <source>Left to right</source>
        <translation type="unfinished">Gauche à droite</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="131"/>
        <source>Right to left</source>
        <translation type="unfinished">Droite à gauche</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="139"/>
        <source>Direction</source>
        <translation type="unfinished">Direction</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="155"/>
        <source>Rows</source>
        <translation type="unfinished">Lignes</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="263"/>
        <source>Number</source>
        <translation type="unfinished">Nombre</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="283"/>
        <source>Numeration type</source>
        <translation type="unfinished">Type de numérotation</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="294"/>
        <source>numeric (1, 2, 3, ...)</source>
        <translation type="unfinished">numérique (1, 2, 3, ...)</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="299"/>
        <source>alpha (A, B, C, ...)</source>
        <translation type="unfinished">alphabétique (A, B, C, ...)</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="307"/>
        <source>Starting number</source>
        <translation type="unfinished">Nombre de départ</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="254"/>
        <source>Columns</source>
        <translation type="unfinished">Colonnes</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="350"/>
        <source>Preview</source>
        <translation type="unfinished">Aperçu</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="40"/>
        <source>Edge size</source>
        <translation type="unfinished">Taille de l&apos;arête</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="360"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot;font-weight:600;&quot;&gt;Warning:&lt;/span&gt; the layer created is temporary.If you want to keep it you&apos;ll have to save it.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot;font-weight:600;&quot;&gt;Attention :&lt;/span&gt; le calque créé est temporaire. Si vous souhaitez le conserver, vous aurez à l&apos;enregistrer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="327"/>
        <source>Numeration order</source>
        <translation type="unfinished">Ordre de numérotation</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="335"/>
        <source>asc (1, 2, 3, ...)</source>
        <translation type="unfinished">asc (1, 2, 3, ...)</translation>
    </message>
    <message>
        <location filename="carroyage_dialog_base.ui" line="340"/>
        <source>desc (3, 2, 1, ...)</source>
        <translation type="unfinished">desc (3, 2, 1, ...)</translation>
    </message>
</context>
</TS>
