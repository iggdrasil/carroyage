# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Carroyage
                                 A QGIS plugin
 This QGIS plugin allows you to generate automatically grids on a map.
                              -------------------
        begin                : 2017-12-16
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Iggdrasil
        email                : etienne.loks@iggdrasil.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load Carroyage class from file Carroyage.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .carroyage import Carroyage
    return Carroyage(iface)
