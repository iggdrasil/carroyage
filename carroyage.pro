SOURCES = __init__.py \
          carroyage_dialog.py \
          carroyage.py

FORMS = carroyage_dialog_base.ui

TRANSLATIONS = i18n/carroyage_en.ts \
               i18n/carroyage_fr.ts