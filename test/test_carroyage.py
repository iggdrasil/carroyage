#!/usr/bin/env python
# coding=utf-8

"""
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
"""
from __future__ import absolute_import

import sip
try:
    sip.setapi('QString', 2)
    sip.setapi('QVariant', 2)
    sip.setapi('QDate', 2)
    sip.setapi('QDateTime', 2)
    sip.setapi('QTextStream', 2)
    sip.setapi('QTime', 2)
    sip.setapi('QUrl', 2)
except AttributeError:
    pass


import unittest
import os
import carroyage


from qgis.core import *
from qgis.gui import *

from .utilities import get_qgis_app
QGIS_APP = get_qgis_app()


APP_ROOT = "/usr"
JSON_VECTOR = os.path.join(os.getcwd(), "test/vector.geojson")

os.environ['QGIS_DEBUG'] = '-1'


class CarroyageTest(unittest.TestCase):
    def setUp(self):
        self.app, self.canvas, self.iface, parent = get_qgis_app()

        self.ref_layer = QgsVectorLayer(JSON_VECTOR, "vector", "ogr")

        # add the layer to the canvas and zoom to it
        QgsMapLayerRegistry.instance().addMapLayer(self.ref_layer)
        self.canvas.setLayerSet([QgsMapCanvasLayer(self.ref_layer)])

        # import the plugin to be tested
        self.plugin = carroyage.classFactory(self.iface)
        self.plugin.test = True
        self.plugin.initGui()
        self.dlg = self.plugin.dlg
        self.dlg.show()

    def test_load_with_no_vector(self):
        """An error is send if there is no selection"""
        self.dlg.preview_button.click()
        self.assertEqual(self.plugin.message_box.isVisible(), True)

    def test_preview(self):
        """A preview is generated when a vector is selected"""
        self.plugin.reference_layer = self.ref_layer
        self.ref_layer.selectByIds([self.ref_layer.getFeatures().next().id()])

        self.dlg.preview_button.click()

        # no error
        self.assertEqual(self.plugin.message_box.isVisible(), False)
        # by default a grid of 10x10 is previewed
        cases = [feat for feat in self.plugin.layer.getFeatures()]
        nb_case_in_grid = len(cases)
        self.assertEqual(nb_case_in_grid, 100)
        # first feature has label '11'
        self.assertEqual(cases[0].attributes()[0], '11')
        # second feature has label '12'
        self.assertEqual(cases[1].attributes()[0], '12')
        # last feature has label '1010'
        self.assertEqual(cases[-1].attributes()[0], '1010')

    def test_parameters(self):
        """Changing parameters in the dialog change grid properties"""
        self.plugin.reference_layer = self.ref_layer
        self.ref_layer.selectByIds([self.ref_layer.getFeatures().next().id()])

        self.dlg.row_nb_input.setValue(5)
        self.dlg.row_type_input.setCurrentIndex(1)
        self.dlg.row_starting_input.setValue(26)
        self.dlg.col_nb_input.setValue(2)
        self.dlg.col_starting_input.setValue(100)
        self.dlg.col_order_input.setCurrentIndex(1)
        self.dlg.preview_button.click()

        # no error
        self.assertEqual(self.plugin.message_box.isVisible(), False)
        # grid of 5x2 is previewed
        cases = [feat for feat in self.plugin.layer.getFeatures()]
        nb_case_in_grid = len(cases)
        self.assertEqual(nb_case_in_grid, 10)
        # first feature has label '100Z'
        self.assertEqual(cases[0].attributes()[0], '100Z')
        # second feature has label '100AA'
        self.assertEqual(cases[1].attributes()[0], '100AA')
        # last feature has label '101AD'
        self.assertEqual(cases[-1].attributes()[0], '99AD')

    """
    def tearDown(self):
        self.plugin.unload()
        # segfault :(
        del(self.plugin)
        del(self.app)
    """


if __name__ == "__main__":
    unittest.main()
